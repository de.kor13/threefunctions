#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstring>

class List;

struct ListNode {
	ListNode * prev= nullptr;
	ListNode * next= nullptr;
	ListNode * rand= nullptr;
	std::string data;
	//ListNode(){std::cout<<"()"<<std::endl;}
	//~ListNode(){std::cout<<"~()"<<std::endl;}

};

class List {
public:
	void Serialize(FILE * file);
	void Deserialize(FILE * file);
	void Add(std::string data);
	void ShowRand();
	void Show();
	int Empty() {
		if (head == nullptr)
			return 0;
		else return 1;
	};
private:
	ListNode * head = nullptr;
	ListNode * tail = nullptr;

	int count;
};

void List::Serialize(FILE * file)
{
	int index = 0;
	ListNode* retPrev = nullptr;

	if (Empty() == 0) return;

	fputc(count, file);
	fputc('\n', file);

	for (ListNode* node = head; node != nullptr; node = node->next,index++)
	{
		
		fputs(node->data.c_str(), file);
		fputc('\n', file);
		*reinterpret_cast<int*> (&(node->prev)) = index;
		
		
	}

	for (ListNode* node = head; node != nullptr; node = node->next)
	{
		if (node->rand != nullptr)
			index = *reinterpret_cast<int*> (&(node->rand->prev));
		else
			index = 0;

			
		fputc(index, file);
		fputc('\n', file);
	}

	for (ListNode* node = head; node != nullptr; node = node->next)
	{
		node->prev = retPrev;
		retPrev = node;
	}



}

void List::Deserialize(FILE * file)
{
	std::string data;
	char buf[225];
	int index, i;
	ListNode* tmp;
	ListNode* retPrev = nullptr;

	int numLines = fgetc(file);
	if (numLines == 0) return;

	fseek(file, 1, SEEK_CUR);

	for (i = 0; i < numLines; i++)
	{
		int j = -1;
		do
		{
			j++;
			buf[j] = fgetc(file);
		} while (buf[j] != '\n');
		buf[j] = '\0';
		data = buf;
		Add(data);
	}

	i = 0;
	for (ListNode* node = head; node != nullptr; node = node->next)
	{
		tmp = node;
		index = fgetc(file);
		fseek(file, 1, SEEK_CUR);
		if (index != 0)
		{
			if (index > i)
				for (int j = 0; j < index; j++)
					tmp = tmp->next;
			else
				for (int j = index; j < i; j++)
					tmp = tmp->prev;
			node->rand = tmp;
		}
		i++;
	}
}

void List::ShowRand()
{
	if (head == nullptr)
	{
		std::cout << "List is empty" << std::endl;
		return;
	}
	for (ListNode* node = head; node != nullptr; node = node->next)
	{
			
		if (node->rand != nullptr){
			std::cout << (node->rand->data).c_str() << std::endl;
		//	node->~ListNode();
		}
		else{
			std::cout << "nullptr" << std::endl;
		} 
	}
}

void List::Add(std::string data) {
	

	ListNode *temp = new ListNode;
	temp->next = nullptr;
	temp->data = data;
	count++;
//	std::cout<<temp->data<<std::endl;

	if (head != nullptr)
	{
		temp->prev = tail;
		tail->next = temp;
		tail = temp;
	//	if (tail->prev != nullptr)
			temp->rand = tail->prev;
	//		std::cout<<temp->rand->data<<std::endl;
	//	else temp->rand = nullptr;

	}
	else
	{
		
		temp->prev = nullptr;
		temp->next = nullptr;
		temp->rand = nullptr;
		head = tail = temp;
		
	
	}
	
}
	void List::Show(){
		if (head == nullptr)
	{
		std::cout << "List is empty" << std::endl;
		return;
	}

		for (ListNode* node = head; node != nullptr; node = node->next)
	{		
			std::cout << node->data.c_str() << std::endl;
	}

	}


void numToBinary(int32_t n){ 
	std::cout<<"binary: ";

	for(int i = sizeof(int)*8 - 1; i >=0; i--)
		std::cout << int((n>>i)&1);

		std::cout << std::endl;
}


void RemoveDups(char* pStr)
{
	if ((pStr == nullptr) || (*pStr == '\0'))
	{
		return;
	}

	unsigned origCursor = 1, editCursor = 0;
	unsigned len = strlen( pStr );


 for ( ; origCursor < len; origCursor++ ){


		if (pStr[origCursor]!=pStr[editCursor])
		{
			pStr[++editCursor]=pStr[origCursor];		
		}

	 }

pStr[ editCursor + 1 ] = '\0';


}




int main()
{

numToBinary(-45);

char test[] = "AAAA  BBB AAA";

	std::cout << "first: " << test << std::endl;
	RemoveDups(test);
	std::cout << "Edited: " << test << std::endl;

const char* path = "input.dat";
	List inputList;
	List outputList;
	FILE *file;
	
	file = fopen(path, "wb");	

	inputList.Add("1");
	inputList.Add("B");
	inputList.Add("-45");
	inputList.Add("77");
	inputList.Add("E5");
	inputList.Add("Pressfs");

	std::cout << "Input" << std::endl;
	inputList.ShowRand();
	//inputList.Show();
	inputList.Serialize(file);
	fclose(file);

	fopen(path, "rb");
	outputList.Deserialize(file);
	std::cout << std::endl << "Output " << std::endl;
	outputList.ShowRand();
	//outputList.Show();
	fclose(file);








return 0;
 
}

